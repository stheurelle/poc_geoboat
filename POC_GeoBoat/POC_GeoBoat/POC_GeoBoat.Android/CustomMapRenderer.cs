﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Runtime;
using POC_GeoBoat.Common;
using POC_GeoBoat.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.Android;
using Image = Android.Media.Image;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace POC_GeoBoat.Droid
{
    public class CustomMapRenderer : MapRenderer/*, GoogleMap.IInfoWindowAdapter*/
    {
        private List<MyZone> _lesZones;
        private List<Pin> _customePins;
        public CustomMapRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                // Unsubscribe
            }

            if (e.NewElement != null)
            {
                //get map
                var formsMap = (CustomMap)e.NewElement;
                //get map zone and stack it in a private list
                _lesZones = formsMap.Zones;
                _customePins = formsMap.Pins.ToList();
                //Get the new map
                Control.GetMapAsync(this);
            }
        }

        protected override void OnMapReady(Android.Gms.Maps.GoogleMap map)
        {
            base.OnMapReady(map);

            foreach (var item in _lesZones)
            {
                //create new polygon
                var polygonOptions = new PolygonOptions();
                //Color
                polygonOptions.InvokeFillColor(0x660000FF);
                //Stroke 
                polygonOptions.InvokeStrokeWidth(2.0f);

                foreach (var position in item.Pins)
                {
                    //Add polygon points
                    polygonOptions.Add(new LatLng(position.Latitude, position.Longitude));
                }

                if (item.Pins.Count > 0)
                {
                    //Add polygon to map
                    NativeMap.AddPolygon(polygonOptions);
                    
                }
            }
        }

        protected override MarkerOptions CreateMarker(Pin pin)
        {
            var marker = new MarkerOptions();
            marker.SetPosition(new LatLng(pin.Position.Latitude, pin.Position.Longitude));
            marker.SetTitle(pin.Label);
            marker.SetSnippet(pin.Address);
            ////change color
            //marker.SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueBlue));

            return marker;
        }

        
    }

}
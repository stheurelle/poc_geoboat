﻿using POC_GeoBoat.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace POC_GeoBoat.ModelViews
{
    public class MenuViewModel : ViewModelBase
    {
        private List<Mission> _missions;
        public MenuViewModel()
        {
            _missions = new List<Mission>
            {
              new Mission{Id=1, Label= "Mission 1"},
              new Mission{Id=2, Label= "Mission 2"},
              new Mission{Id=3, Label= "Mission 3"}
            };

            Missions = new ObservableCollection<Mission>(_missions);
        }

        private ObservableCollection<Mission> missions;
        public ObservableCollection<Mission> Missions
        {
            get { return missions; }
            set
            {
                missions = value;
                RaisePropertyChanged("Missions");
            }
        }

        private Mission _missionSelected;
        public  Mission MissionSelected
        { 
            get { return _missionSelected; }
            set {
                _missionSelected = value;
                RaisePropertyChanged("MissionSelected");
                GoToMissionPage(value);
                
            }
        }

        
        //TODO: ajouter la navigation 
        private  void GoToMissionPage(Mission mission)
        {
            if (App.Current.MainPage != null && mission != null)
            {
                 App.Current.MainPage.DisplayAlert("ok", mission.Label, "ok");
            }
        }
        
        
        #region Command => Logout
        private ICommand _logoutCommand;
        public ICommand LogoutCommand
        {
            get
            {
                _logoutCommand = _logoutCommand ?? new RelayCommand(DoLogoutCommand);
                return _logoutCommand;
            }
        }
        private void DoLogoutCommand(object param)
        {
            if (App.Current.MainPage != null)
            {
                App.Current.MainPage.DisplayAlert("ok", "logout", "ok");
            }
        }
        #endregion
        
        #region Command => Anomalie
        private ICommand _toAnomalie;
        public ICommand ToAnomalie
        {
            get
            {
                _toAnomalie = _toAnomalie ?? new RelayCommand(GoToAnomalie);
                return _toAnomalie;
            }
        }
        private void GoToAnomalie(object param)
        {
            if (App.Current.MainPage != null)
            {
                App.Current.MainPage.DisplayAlert("ok", "Anomalie", "ok");
            }
        }
        #endregion

        #region Command => Statut
        private ICommand _toStatuts;
        public ICommand ToStatut
        {
            get
            {
                _toStatuts = _toStatuts ?? new RelayCommand(GoToStatut);
                return _toStatuts;
            }
        }
        private void GoToStatut(object param)
        {
            if (App.Current.MainPage != null)
            {
                App.Current.MainPage.DisplayAlert("ok", "Status", "ok");
            }
        }
        

        #endregion
        
        #region Command => Menu
        private ICommand _toMenu;
        public ICommand ToMenu
        {
            get
            {
                _toMenu = _toMenu ?? new RelayCommand(GoToMenu);
                return _toMenu;
            }
        }
        private void GoToMenu(object param)
        {
            if (App.Current.MainPage != null)
            {
                App.Current.MainPage.DisplayAlert("ok", "Menu", "ok");
            }
        }
        

        #endregion
    }
}

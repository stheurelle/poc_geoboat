﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POC_GeoBoat.ModelViews;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POC_GeoBoat.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StatutPage : ContentPage
	{
		public StatutPage ()
		{
			InitializeComponent ();
			var model = new StatutViewModel();
			BindingContext = model;
		}
	}
}
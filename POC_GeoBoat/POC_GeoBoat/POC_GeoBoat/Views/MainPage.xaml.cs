﻿using Newtonsoft.Json;
using Plugin.Geolocator;
using Plugin.Permissions.Abstractions;
using POC_GeoBoat.Common;
using POC_GeoBoat.ModelViews;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Button = Xamarin.Forms.Button;
using Position = Xamarin.Forms.Maps.Position;


namespace POC_GeoBoat.Views
{
    public partial class MainPage : ContentPage
    {
        #region var & prop

        private Entry _latitude = new Entry { HorizontalOptions = LayoutOptions.FillAndExpand };
        private Entry _longitude = new Entry { HorizontalOptions = LayoutOptions.FillAndExpand };
        private readonly CustomMap _customMap;
        private CustomMap _finalMap;
        private Plugin.Geolocator.Abstractions.Position _currentPosition;

        private List<MyZone> _lesZones = new List<MyZone>();
        private double _currentLat;
        private double _currentLon;


        private MyZone _enteredZone;

        #endregion

        #region CTOR
        public MainPage()
        {
            InitializeComponent();
            var model = new MainViewModel();
            BindingContext = model;

            //Create map
            _customMap = new CustomMap
            {
                MapType = MapType.Street,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };

            #region UI
            stack = new StackLayout { Spacing = 0 };
            stack.Children.Add(_customMap);
            Content = stack;


            var btnAutoPin = new Button { Text = "Auto Pin", HorizontalOptions = LayoutOptions.FillAndExpand };
            var btnClear = new Button { Text = "Position", HorizontalOptions = LayoutOptions.FillAndExpand, IsVisible = false };
            var stackAutoPin = new StackLayout { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.FillAndExpand };
            var btnIsIn = new Button { Text = "In Zone", HorizontalOptions = LayoutOptions.FillAndExpand, IsVisible = false };
            stackAutoPin.Children.Add(btnAutoPin);
            stackAutoPin.Children.Add(btnClear);
            //stackAutoPin.Children.Add(btnIsIn);
            stack.Children.Add(stackAutoPin);

            btnAutoPin.Clicked += BtnAutoPin_Clicked;
            btnClear.Clicked += BtnClear_Clicked;
            #endregion

        }
        #endregion

        #region LocationChanged_Event
        [SuppressMessage("ReSharper", "FunctionRecursiveOnAllPaths")]
        private async void LocationChanged_Event()
        {
            IsIn();
            await Task.Delay(2500);
            //display the name of the zone 
            if (_enteredZone != null)
            {
                await DisplayAlert("Event", "You are in zone : " + _enteredZone.Libelle, "ok");
            }

            await Task.Delay(2500);

            LocationChanged_Event();
        }


        #endregion

        #region Set & MoveTo Polygon
        //SET
        private void BtnAutoPin_Clicked(object sender, EventArgs e)
        {
            AutoPin();
        }

        private void AutoPin()
        {
            const string path = "POC_GeoBoat.Files.Position_1.json";
            var deserializer = new Deserializer();
            _lesZones = deserializer.DeserializeFromFilesToZones(path);

            if (customMap.Zones.Select(z => z.Pins).ToList().Count > 0)
            {
                _customMap.Zones.Select(z => z.Pins).ToList().Clear();
            }

            //Get zones from json
            foreach (var item in _lesZones)
            {
                _customMap.Libelle = item.Libelle;

                _customMap.Zones.Add(
                    new MyZone
                    {
                        Libelle = item.Libelle,
                        Pins = item.Pins
                    });


                _finalMap = new CustomMap
                {
                    MapType = MapType.Street,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Zones = _customMap.Zones
                };
            }

            #region UI
            stack.Children.Clear();
            stack.Children.Add(_finalMap);
            var btnAutoPin = new Button { Text = "Auto Pin", HorizontalOptions = LayoutOptions.FillAndExpand };
            var btnClear = new Button { Text = "Location", HorizontalOptions = LayoutOptions.FillAndExpand, IsVisible = false };
            var btnIsIn = new Button { Text = "In Zone", HorizontalOptions = LayoutOptions.FillAndExpand, IsVisible = false };
            var stackAutoPin = new StackLayout { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.FillAndExpand };
            stackAutoPin.Children.Add(btnAutoPin);
            stack.Children.Add(stackAutoPin);

            btnAutoPin.Clicked += BtnAutoPin_Clicked;
            btnClear.Clicked += BtnClear_Clicked;
            btnIsIn.Clicked += BtnIsIn_Clicked;

            Content = stack;
            #endregion

            ClearPin();
        }
        //MOVETO

        private void ClearPin()
        {
            //Move to desired location
            var item = _finalMap.Zones.Select(l => l).First();
            var span = MapSpan.FromCenterAndRadius(new Position(item.Pins.First().Latitude, item.Pins.First().Longitude), Distance.FromMiles(0.3));
            _finalMap.MoveToRegion(span);
            _customMap.Pins.Clear();
            
            LocationChanged_Event();
        }
        #endregion

        #region Check if device is in an area

        private bool _isIn;
        private void BtnIsIn_Clicked(object sender, EventArgs e)
        {
            IsIn();
        }

        private async void IsIn()
        {
            await GetCurrentPosition();

            if (_currentPosition == null) return;

            //Check if device location is in the area
            var libelleZone = "";
            foreach (var item in _finalMap.Zones)
            {
                var c = false;
                var i = 0;
                var j = item.Pins.Count - 1;
                foreach (var point in item.Pins)
                {
                    var latI = point.Latitude;
                    var lonI = point.Longitude;

                    var latJ = item.Pins[j].Latitude;
                    var lonJ = item.Pins[j].Longitude;

                    if (((latI > _currentPosition.Latitude != (latJ > _currentPosition.Latitude)) &&
                         (_currentPosition.Longitude < (lonJ - lonI) * (_currentPosition.Latitude - latI) / (latJ - latI) + lonI)))
                        c = !c;
                    j = i++;
                }

                libelleZone = item.Libelle;
                _isIn = c;
                if (_isIn)
                {
                    //enteredZone is used by LocationChanged_Event()
                    _enteredZone = item;
                    break;
                }
                else
                {
                    _enteredZone = null;
                }
            }


        }
        #endregion

        #region Get Current Position
        private async void BtnClear_Clicked(object sender, EventArgs e)
        {
            await GetCurrentPosition();

        }
        public async Task<Plugin.Geolocator.Abstractions.Position> GetCurrentPosition()
        {
            //Check device permission
            var hasPermission = await Utils.CheckPermissions(Permission.Location);

            var position = new Plugin.Geolocator.Abstractions.Position();
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 100;

                //If position was already knew
                position = await locator.GetLastKnownLocationAsync();
                if (position != null)
                {
                    _currentPosition = new Plugin.Geolocator.Abstractions.Position
                    {
                        Latitude = position.Latitude,
                        Longitude = position.Longitude
                    };
                }

                if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
                {
                    return null;
                }

                position = await locator.GetPositionAsync(TimeSpan.FromSeconds(30), null);

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Unable to get location: " + ex);
            }

            if (position != null)
            {
                try
                {
                    _currentPosition = new Plugin.Geolocator.Abstractions.Position
                    {
                        Latitude = position.Latitude,
                        Longitude = position.Longitude
                    };
                    _finalMap.Pins.Clear();
                    _finalMap.Pins.Add(new Pin
                    {
                        Label = "Current Position",
                        Position = new Position(position.Latitude, position.Longitude),
                        Type = PinType.SearchResult
                    });
                    return _currentPosition;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return null;
                }
            }
            else
            {
                return null;
            }



        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POC_GeoBoat.Common;
using POC_GeoBoat.ModelViews;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POC_GeoBoat.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        //private var model;
        public MenuPage()
        {
            InitializeComponent();
            var model = new MenuViewModel();
            BindingContext = model;

            lstMissions.ItemTapped += LstMissions_ItemTapped;
        }

        private void LstMissions_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            // don't do anything if we just de-selected the row
            if (e.Item == null) return;
            // do something with e.SelectedItem
            ((ListView)sender).SelectedItem = null; // de-select the row
        }
    }
}
﻿using System.Collections.Generic;
using Xamarin.Forms.Maps;

namespace POC_GeoBoat.Common
{
    public interface IMapRenderer
    {
        void Render(List<Position> shapeCoordinates);

        void Dessin();
    }
}
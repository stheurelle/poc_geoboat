﻿namespace POC_GeoBoat.Common
{
    public class MyPin
    {
        public MyPin(double latitude, double longitude, string label)
        {
            Latitude = latitude;
            Longitude = longitude;
            Label = label;
        }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Label { get; set; }


    }
}

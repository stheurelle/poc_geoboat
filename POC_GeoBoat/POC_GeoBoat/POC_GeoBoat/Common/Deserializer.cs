﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;

namespace POC_GeoBoat.Common
{
    public class Deserializer
    {
        public List<MyZone> DeserializeFromFilesToZones(string path)
        {
            var assembly = typeof(App).GetTypeInfo().Assembly;

            var stream = assembly.GetManifestResourceStream(path);
            var reader = new StreamReader(stream);
            var flux = reader.ReadToEnd();
            var lesZones = JsonConvert.DeserializeObject<List<MyZone>>(flux);

            return lesZones;
        }

        public List<dynamic> DeserializeFromUrlToGeneric(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return null;
            }

            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();
            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                var content = reader.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(content))
                {
                    var list = JsonConvert.DeserializeObject<dynamic>(content);
                    return new List<dynamic>();
                }
                else
                {
                    return null;
                }
            }

        }
    }
}

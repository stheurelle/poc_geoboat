﻿using System.Collections.Generic;

namespace POC_GeoBoat.Common
{
    public class MyZone
    {
        public List<MyPin> Pins { get; set; }
        public string Libelle { get; set; }
    }
}

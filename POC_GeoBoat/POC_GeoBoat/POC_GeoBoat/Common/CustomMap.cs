﻿using System.Collections.Generic;
using Xamarin.Forms.Maps;

namespace POC_GeoBoat.Common
{
    public class CustomMap : Map
    {
        public List<MyZone> Zones;
        public string Libelle;

        public CustomMap()
        {
                Zones = new List<MyZone>();
        }
    }
}

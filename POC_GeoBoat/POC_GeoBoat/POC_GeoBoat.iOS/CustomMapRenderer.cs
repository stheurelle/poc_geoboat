﻿using System.Collections.Generic;
using CoreLocation;
using MapKit;
using ObjCRuntime;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Platform.iOS;
using POC_GeoBoat.Common;
using POC_GeoBoat.iOS;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace POC_GeoBoat.iOS
{
    public class CustomMapRenderer : MapRenderer
    {
        private MKPolygonRenderer _polygonRenderer;
        private List<MyZone> _lesZones;

        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                var nativeMap = Control as MKMapView;
                if (nativeMap == null) return;
                {
                    nativeMap.RemoveOverlays(nativeMap.Overlays);
                    nativeMap.OverlayRenderer = null;
                    _polygonRenderer = null;
                }

                if (e.NewElement != null)
                {
                    var formsMap = (CustomMap) e.NewElement;
                    var nativeMap2 = (MKMapView) Control;
                    _lesZones = formsMap.Zones;
                   nativeMap2.OverlayRenderer = GetOverlayRenderer;

                    foreach (var item in _lesZones)
                    {
                        CLLocationCoordinate2D[] coords = new CLLocationCoordinate2D[item.Pins.Count];

                        int index = 0;
                        foreach (var point in item.Pins)
                        {
                            coords[index] = new CLLocationCoordinate2D(point.Latitude, point.Longitude);
                            index++;
                        }

                        var blockOverlay = MKPolygon.FromCoordinates(coords);
                        nativeMap.AddOverlay(blockOverlay);
                    }

                   
                }
            }
        }

        MKOverlayRenderer GetOverlayRenderer(MKMapView mapView, IMKOverlay overlayWrapper)
        {
            if (_polygonRenderer == null && !Equals(overlayWrapper, null))
            {
                var overlay = Runtime.GetNSObject(overlayWrapper.Handle) as IMKOverlay;
                _polygonRenderer = new MKPolygonRenderer(overlay as MKPolygon)
                {
                    FillColor = UIColor.Red,
                    StrokeColor = UIColor.Blue,
                    Alpha = 0.4f,
                    LineWidth = 9
                };
            }
            return _polygonRenderer;
        }
    }
}